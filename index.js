const MongoClient = require('mongodb').MongoClient

const {
  MONGO_URL,
  MONGO_DB_NAME,
  MONGO_COLLECTION_NAME
} = process.env

const LIMIT_SIZE = 16 * 1024 * 1024
const SIZE_OF_NUMBER = 8 // Number impl by `double`, which is 8 bytes

async function overflow () {
  const client = await MongoClient.connect(MONGO_URL)
  const db = client.db(MONGO_DB_NAME)
  const testCollection = db.collection(MONGO_COLLECTION_NAME)
  const result = await testCollection.find({id: {$in: new Array(LIMIT_SIZE / SIZE_OF_NUMBER)}}).toArray()
  return result
}

overflow().then(console.log).catch(console.error)
